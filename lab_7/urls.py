from django.conf.urls import url
from .views import index, add_friend, validate_npm, delete_friend, friend_list, get_page_data_by_page_number, get_friend_data

urlpatterns = [
     url(r'^$', index, name='index'),
     url(r'^add-friend/$', add_friend, name='add-friend'),
     url(r'^validate-npm/$', validate_npm, name='validate-npm'),
     url(r'^delete-friend/$', delete_friend, name='delete-friend'),
     url(r'^get-friend-data/$', get_friend_data, name='get-friend-data'),
     url(r'^get-friend-list/$', friend_list, name='get-friend-list'),
     url(r'^get-students-by-page/(?P<page_number>[0-9]+)/$', get_page_data_by_page_number)
]
