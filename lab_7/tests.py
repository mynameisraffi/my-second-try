# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase, Client
from django.urls import resolve
from django.core import serializers
import os
import json

from .views import index
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper

class Lab7UnitTest(TestCase):
    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    def test_friend_list_url_is_exist(self):
        response = Client().get('/lab-7/get-friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_can_get_client_id(self):
        csui_helper = CSUIhelper()
        self.assertEqual(csui_helper.instance.get_client_id(), 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')

    def test_can_get_auth_param_dict(self):
        csui_helper = CSUIhelper()
        auth_dict = csui_helper.instance.get_auth_param_dict()
        self.assertTrue(isinstance(auth_dict, dict))
        self.assertEqual(auth_dict["client_id"], csui_helper.instance.get_client_id())

    def test_can_get_list_mahasiswa_by_page(self):
        csui_helper = CSUIhelper()
        mahasiswa_list, next_page, prev_page = csui_helper.instance.get_mahasiswa_list_by_page(1)
        self.assertEqual('', prev_page)
        self.assertEqual('2', next_page)
        self.assertTrue(isinstance(mahasiswa_list, list))

    def test_extract_number_will_return_1(self):
        csui_helper = CSUIhelper()
        num = csui_helper.instance.get_page_number_from_url("http://google")
        self.assertEqual('1', num)

    def test_invalid_sso(self):
        csui_helper = CSUIhelper()
        csui_helper.instance.username = "asd"
        csui_helper.instance.password = "asd"
        with self.assertRaisesMessage(Exception, 'asd'):
            csui_helper.instance.get_access_token()

    def test_add_friend(self):
        response_post = Client().post(
            '/lab-7/add-friend/',
            {'name': "test", 'npm': "1234"}
        )
        self.assertEqual(response_post.status_code, 200)

    def test_cant_add_friend(self):
        response_post = Client().post(
            '/lab-7/add-friend/',
            {'name': "test", 'npm': "1234"}
        )

        response_post = Client().post(
            '/lab-7/add-friend/',
            {'name': "test", 'npm': "1234"}
        )
        self.assertEqual(response_post.status_code, 400)

    def test_validate_npm(self):
        response = self.client.post('/lab-7/validate-npm/')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(html_response, {'is_taken': False})

        response = Client().post(
            '/lab-7/add-friend/',
            {'name': 'test', 'npm': '1234'}
        )

        response = self.client.post('/lab-7/validate-npm/', {'npm': '1234'})
        html_response = response.content.decode('utf8')
        self.assertEqual(200, response.status_code)
        self.assertJSONEqual(html_response, {'is_taken': True})

    def test_delete_friend(self):
        friend = Friend.objects.create(
            friend_name="test",
            npm="1234"
        )
        response = Client().post(
            '/lab-7/delete-friend/', {"id": friend.pk}
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(0, Friend.objects.all().count())

    def test_delete_method_wont_accept_non_POST_method(self):
        get = Client().get('/lab-7/delete-friend/')
        delete = Client().delete('/lab-7/delete-friend/', {"id": '1'})

        self.assertEqual(400, get.status_code)
        self.assertEqual(400, delete.status_code)

    def test_get_mahasiswa_data_by_page(self):
        csui_helper = CSUIhelper()
        mahasiswa_list, next_page, prev_page = csui_helper.instance.get_mahasiswa_list_by_page(2)
        data = {
            'next_page_num': next_page,
            'prev_page_num': prev_page,
            'mahasiswa_list': mahasiswa_list,
        }

        response = Client().get('/lab-7/get-students-by-page/2/')
        self.assertJSONEqual(response.content.decode('utf8'), data)

    def test_can_get_friend_data(self):
        response_post = Client().post(
            '/lab-7/add-friend/',
            {'name': "test", 'npm': "1234"}
        )

        friend_data = Client().get('/lab-7/get-friend-data/')
        html_response = friend_data.content.decode('utf8')

        data = serializers.serialize('json', Friend.objects.all())
        struct = json.loads(data)
        data = []
        for instance in struct:
            obj = {
                'id': instance['pk'],
                'data': instance['fields'],
            }
            data.append(obj)
        self.assertJSONEqual(html_response, {'data': data})
