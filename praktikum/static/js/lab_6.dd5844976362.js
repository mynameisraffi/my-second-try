$( document ).ready(function() {
  mySelect = $('.my-select').select2();

  var tema = '[\
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},\
    {"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},\
    {"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},\
    {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},\
    {"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},\
    {"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},\
    {"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},\
    {"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},\
    {"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},\
    {"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},\
    {"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}\
  ]';

  localStorage.setItem("tema", tema);

  mySelect.select2({
    'data' : JSON.parse(localStorage.getItem("tema"))
  });

  if(!localStorage.selectedTheme) {
    localStorage.selectedTheme = JSON.stringify({"bcgColor":"#3F51B5","fontColor":"#FAFAFA"});
  } else {
    $("body").css('background-color', JSON.parse(localStorage.selectedTheme["bcgColor"]);
    $("text-center").css('color', JSON.parse(localStorage.selectedTheme["fontColor"]);
  }

  $('.my-select').select2({
    'data': JSON.parse(localStorage.themes);
  });

  $('.apply-button').on('click', function(){
    var bcg = $(".my-select").select2("data")[0]["bcgColor"];
    var font = $(".my-select").select2("data")[0]["fontColor"];

    var temp = JSON.stringify({"bcgColor" : bcg, "fontColor" : font});

    localStorage.selectedTheme = temp;

    $("body").css('background-color', JSON.parse(localStorage.selectedTheme)["bcgColor"]);
    $(".text-center").css('color', JSON.parse(localStorage.selectedTheme)["fontColor"]);
  });

});


// Chat
var isian = "";
var tempatChat = document.getElementById("tempat-chat");
var chat = function(event) {
  if(event.keyCode === 13) {
    tempatChat.value = "";
    var kotakChat = document.createElement("P");
    var isiChat = document.createTextNode(isian);
    var box = document.getElementsByClassName("msg-insert")[0];
    kotakChat.setAttribute("class", "msg-send");
    kotakChat.appendChild(isiChat);
    box.appendChild(kotakChat);
    isian = "";
  }
  else {
    isian += event.key;
    console.log(isian);
  }
};

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    print.value = "";
    erase = true;
  } else if (x === 'eval') {
    print.value = Math.round(evil(print.value) * 10000) / 10000;
    erase = true;
  } else if (x === 'log') {
    print.value = Math.log10(print.value);
  } else if (x === 'sin') {
    print.value = Math.sin(print.value * Math.PI / 180);
    if(print.value == 1.2246467991473532e-16) print.value = 0;
  } else if (x === 'tan') {
    print.value = Math.tan(print.value * Math.PI / 180);
    if(print.value == -1.2246467991473532e-16) print.value = 0;
    else if(print.value == 0.9999999999999999) print.value = 1;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
};
